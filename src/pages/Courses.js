// import courses from '../data/courses';
import { useEffect, useState, useContext } from 'react';
import CourseCard from '../components/CourseCard';
import UserContext from '../UserContext'


export default function Courses (){

	// check to see if the mock data was captured
	// console.log(courses);
	// console.log(courses[0]);


	const [courses, setCourses] = useState([]);

	const {user} = useContext(UserContext);

	useEffect(()=>{

		fetch(`http://localhost:4000/courses`)
		.then(res=>res.json())
		.then(data=>{

			const courseArr = (data.map(course=>{

				return (

					<CourseCard courseProp={course} key={course._id}/>

				)

			}))
			setCourses(courseArr)
		})
	},[courses])

	// const coursesMap = courses.map(course => {
	// 	return (
	// 		<CourseCard key={course.id} courseProp={course}/>
	// 	)
	// })

	return(

		<>
			<h1>Courses</h1>
			{courses}
		</>
	)
}