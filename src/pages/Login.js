import { useState, useEffect, useContext } from 'react';
import { Form, Button } from 'react-bootstrap';
import { Navigate } from 'react-router-dom'
import Swal from 'sweetalert2';
import UserContext from '../UserContext';

export default function Login() {

	const { user, setUser } = useContext(UserContext);

	const [email, setEmail] = useState("");
	const [password1, setPassword1] = useState("");

	const [isActive, setIsActive] = useState(false);


	function authenticate(e){

		e.preventDefault();

		fetch('http://localhost:4000/users/login',{
			method: 'POST',
			headers:{
				'Content-Type':'application/json'
			},
			body: JSON.stringify({
				email: email,
				password: password1
			})
		})
		.then(res=>res.json())
		.then(data=>{
			console.log(data)

			if (typeof data.access !== "undefined"){
				localStorage.setItem('token',data.access)
				retrieveUserDetails(data.access)

				Swal.fire({
					title: "Login Successful!",
					icon: "success",
					text: "Welcome to Booking App of 248!"
				})
			} else {

				Swal.fire({
					title: "Authentication Unsuccessful!",
					icon: "error",
					text: "Check you credentials!"
				})
			}
		})

		const retrieveUserDetails = (token) => {

			fetch('http://localhost:4000/users/details',{
				headers:{
					Authorization: `Bearer ${token}`
				}
			})
			.then(res=>res.json())
			.then(data=>{
				console.log(data);

				setUser({
					id: data._id,
					isAdmin: data.isAdmin
				})
			})
		}


		// comment out to allow the page to render automatically after the submit button is triggered


		//set the eamil of the authenticated user in the local storage
		/*
			Syntax:
				localStorage.setItem("propertyName", value)

		*/

		// localStorage.setItem("email",email);

		// setUser({email: localStorage.getItem('email')})

		setEmail("");
		setPassword1("");

		//alert(`Welcome back, ${email}!`)

	}


	useEffect(()=>{

		if(email !== "" && password1 !== ""){
			setIsActive(true)
		}else{
			setIsActive(false)
		}

	},[email, password1])


	return (

	(user.id !== null) ?
	<Navigate to="/courses"/>
	
	:

	<>
		<h1>Login</h1>

		<Form onSubmit={(e)=>authenticate(e)}>

	      <Form.Group className="mb-3" controlId="userEmail">
	        <Form.Label>Email Address</Form.Label>
	        <Form.Control
	        	type="email"
	        	placeholder="Enter Email"
	        	value = {email}
	        	onChange = {e=>setEmail(e.target.value)}
	        	required/>
	        <Form.Text className="text-muted">
	          We'll never share your email with anyone else.
	        </Form.Text>
	      </Form.Group>

	      <Form.Group className="mb-3" controlId="password1">
	        <Form.Label>Password</Form.Label>
	        <Form.Control
	        	type="password"
	        	placeholder="Password"
	        	value = {password1}
	        	onChange = {e=>setPassword1(e.target.value)}
	        	required/>
	      </Form.Group>

	      {isActive ?

	      	<Button variant="success" type="submit" id="submitBtn">
	       		Submit
	      	</Button>

	      	:

	      	<Button variant="secondary" type="submit" id="submitBtn" disabled>
	        	Submit
	      	</Button>

	      }

	    </Form>
	 </>
	)
}