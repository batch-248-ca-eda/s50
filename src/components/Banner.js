// import Row from 'react-bootstrap/Row';
// import Col from 'react-bootstrap/Col';

import { Row, Col, Button } from "react-bootstrap";
import { Link } from 'react-router-dom';

export default function Banner({data}){

	console.log(data);
	const { title, content, destination, label } = data;
	return(

		<Row>
			<Col>
				<h1>{title}</h1>
				<p>{content}</p>
				<Button variant="primary" as={Link} to={destination}>{label}</Button>
			</Col>
		</Row>

	)


}
