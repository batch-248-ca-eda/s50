Creating React app through this command:
			npx create-react-app <appName>
-if an error is encountered:
			npm uninstall -g create-react-app
-if the error is create-react-app command not found
			>> npm install create-react-app

For Clean Slate:
	1. Remove the following files
		App.test.js
		index.css
		logo.svg
		reportWebVitals.js

	2. Remove the importations in index.js such as the css and reportWebVitals

	3. Remove the importation of logo in App.js and the return div


Installing Javascript(Babel)-code readability

	1. ctrl + shift + p
	2. type install
	3. select Package Control: Install Package
	4. On the lower left, you will see loading repositories
	5. type in babel
	6. select the first option
	7. once installed, on the lower right of your sublime editor, find the languages available and click Javascript(Babel) or JSX

JSX (JavaScript XML)
HTML-like codes
The difference between the HTML codes is that you can insert a Javascript logic in it.

	>HTML<

		<button disabled></button>

	>JSX<

		<button {if(){
		}
		else{

		}></button>

In React, you may also install dependencies through npm such bootstrap and react-bootstrap
	Command with versions: npm i bootstrap@5.1.3 react-bootstrap@2.0.2
		You may also downgrade your version installation
		Recommended: use the LTS version
